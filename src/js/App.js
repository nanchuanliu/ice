import React, { Component } from 'react';
import logo from '../img/logo.svg';
import '../css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <a
            className="App-link"
            href="/love"
            target="_blank"
            rel="noopener noreferrer"
          >
            爱在旅程
          </a>
          <a
              className="App-link"
              href="/jenkins"
              target="_blank"
              rel="noopener noreferrer"
          >
            发布后台
          </a>
        </header>
      </div>
    );
  }
}

export default App;
